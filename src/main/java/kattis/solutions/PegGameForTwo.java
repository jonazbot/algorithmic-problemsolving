package kattis.solutions;

import kattis.library.Kattio;

public class PegGameForTwo {
    private final Kattio io;
    private final int[][] moves;

    public PegGameForTwo() {
        io = new Kattio(System.in);
        moves = new int[][] {   new int[]{-1,-1},
                                new int[]{-1, 0},
                                new int[]{ 0,-1},
                                new int[]{ 0, 1},
                                new int[]{ 1, 0},
                                new int[]{ 1, 1} };
    }

    public int play(int[][] board, boolean jacquez) {
        int bestScore = jacquez ? Integer.MIN_VALUE : Integer.MAX_VALUE;
        int jumpee;
        boolean canMove = false;
        for(int y = 0; y < 5; ++y) {
            for(int x = 0; x <= y; ++x) {
                if(board[y][x] == 0) { continue; }  // Empty hole can't be moved.
                for(int move = 0; move < 6; ++move) {
                    int yNext = 2 * moves[move][0] + y;
                    int xNext = 2 * moves[move][1] + x;

                    // Check for valid move
                    if(!inBounds(yNext, xNext)) { continue; }
                    if(board[y + moves[move][0]][x + moves[move][1]] == 0) { continue; }
                    if(board[yNext][xNext] != 0) { continue; }
                    canMove = true;

                    // Move peg
                    board[yNext][xNext] = board[y][x];
                    board[y][x] = 0;

                    // Remove peg jumped
                    jumpee = board[y + moves[move][0]][x + moves[move][1]];
                    board[y + moves[move][0]][x + moves[move][1]] = 0;

                    // Alternate players
                    if (jacquez) { bestScore = Integer.max(bestScore,jumpee * board[yNext][xNext] + play(board,false)); }
                    else { bestScore = Integer.min(bestScore, -(jumpee * board[yNext][xNext]) + play(board,true)); }

                    // Revert move after recursion call return
                    board[y][x] = board[yNext][xNext];
                    board[yNext][xNext] = 0;
                    board[y + moves[move][0]][x + moves[move][1]] = jumpee;
                }
            }
        }
        if(!canMove) { return 0; }  // Can't move, no more points.
        return bestScore;
    }

    private boolean inBounds(int y, int x) { return x >= 0 && y >= 0 && y < 5 && x <= y; }

    public int[][] readBoard() {
        int[][] board = new int[5][];
        for (int y = 0; y < 5; ++y) {
            int[] nums = new int[y+1];
            for (int x = 0; x <= y; ++x) { nums[x] = io.getInt(); }
            board[y] = nums;
        }
        return board;
    }

    public static void main(String[] args) {
        PegGameForTwo game = new PegGameForTwo();
        System.out.println(game.play(game.readBoard(),true));
    }

}
