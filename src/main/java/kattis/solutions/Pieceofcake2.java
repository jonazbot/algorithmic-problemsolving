// Pieceofcake2.java
// Cold-puter Science - open.kattis.com/problems/cold
// By Jonas Valen

import java.util.*;

public class Pieceofcake2 {
    public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int x = in.nextInt();
		int y = in.nextInt();
		System.out.println(Math.max(x, n - x) * Math.max(y, n - y) * 4);
    }
}
