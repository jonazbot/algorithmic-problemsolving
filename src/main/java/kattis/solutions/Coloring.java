package kattis.solutions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Find the exact chromatic number of a graph.
 *
 * Tree: = 2
 * Cycles: Odd = 3, Even = 2
 * Complete Graph: = nodes
 * Clique: >= clique
 *
 */
public class Coloring {

    private final int nodes, second;
    private final Map<Integer, Integer> colors;
    private final Map<Integer, List<Integer>> graph;

    public Coloring() throws IOException {
        final BufferedReader io = new BufferedReader(new InputStreamReader(System.in));
        this.nodes = Integer.parseInt(io.readLine());
        this.graph = new HashMap<>(this.nodes);
        for (int i = 0; i < this.nodes; ++i) {
            this.graph.put(i, new ArrayList<>());
            String line = io.readLine();
            for (String str : line.split(" ")) { this.graph.get(i).add(Integer.parseInt(str)); }
        }
        this.second = graph.get(0).get(0);
        this.colors = new HashMap<>(this.nodes);
        this.colors.put(0, 0);
        this.colors.put(second, 1);
        System.out.println(findK());
    }

    /**
     * Constructor for JUnit testing.
     *
     * @param nodes The number of vertices/nodes in the graph.
     * @param graph The edge Map of the graph.
     * @param colors The empty Map to color the graph nodes.
     */
    public Coloring(int nodes, Map<Integer, List<Integer>> graph, Map<Integer, Integer> colors) {
        this.nodes = nodes;
        this.graph = graph;
        this.second = graph.get(0).get(0);
        this.colors = colors;
        this.colors.put(0, 0);
        this.colors.put(second, 1);
        System.err.println("0=0, "+second+"=1");
    }

    /**
     * Check if the graph is a complete graph.
     *
     * @return true if the graph is complete, false otherwise.
     */
    private boolean isComplete() {
        for (List<Integer> adjacent : this.graph.values()) { if (adjacent.size() != this.nodes - 1) { return false; } }
        return true;
    }

    /**
     * Find the chromatic number by first checking for a complete graph, then
     * checks when the graph is k-colorable starting at k = 2.
     *
     * @return The chromatic number of the graph.
     */
    public int findK() {
        if (isComplete()) {
            //System.err.println("\n*** Complete Graph "+this.nodes+"-Colorable ***");    // TODO: REMOVE!!!
            return this.nodes; }
        for (int kColors = 2; kColors < this.nodes-1; ++kColors) {
            //System.err.println("\nTRYING K = "+kColors);   // TODO: REMOVE!!!
            if (kColorable(kColors,1)) {
                //System.err.println("\n*** "+kColors+"-Colorable ***"); for (int key: colors.keySet()) { System.err.println(key+": "+colors.get(key)); }  // TODO: REMOVE!!!
                return kColors;
            }
        }
        //System.err.println("\n*** "+(this.nodes-1)+"-Colorable ***"); for (int key: colors.keySet()) { System.err.println(key+": "+colors.get(key)); }  // TODO: REMOVE!!!
        return this.nodes-1;
    }

    /**
     * Checks if k colors can be used to color graph.
     *
     * @param kColors Number of colors allowed.
     * @param next Number of colors used so far.
     * @return true if the graph is k-colorable, otherwise false.
     */
    private boolean kColorable(int kColors, int next) {
        if (next == this.second) { ++next; }
        if (next == this.nodes) { return true; }
        for (int color = 0; color < kColors; ++color) {
            //System.err.println("Next: "+next+" Color: "+color); // TODO: REMOVE!!!
            boolean trueColoring = true;
            for (int node : this.graph.get(next)) {
                //System.err.println("Node "+node); // TODO: REMOVE!!!
                if (node < next && this.colors.get(node) == color) {    //
                    //System.err.println("Node "+node+" is already color "+colors.get(node)); // TODO: REMOVE!!!
                    trueColoring = false;
                    break;
                }
            }
            if (trueColoring) {
                this.colors.put(next, color);
                //System.err.println("Colored Node "+next+" Color: "+color); // TODO: REMOVE!!!
                if (kColorable(kColors,next+1)) { return true; }
            }
        }
        return false;
    }

    public static void main(String[] args) throws IOException { new Coloring(); }
}
