import java.util.*;

public class FizzBuzz {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int x = in.nextInt();
		int y = in.nextInt();
		int n = in.nextInt();
		for(int i = 1; i <= n; i++) {
			
			if (i % x != 0 && i % y != 0) {
				System.out.print(i);
			}
			if (i % x == 0) {
				System.out.print("Fizz");
			}
			if (i % y == 0) {
				System.out.print("Buzz");			
			}
			System.out.println();
		}
	}
}
