// Guess.java
// Guess the Number - uib.kattis.com/problems/guess
// By Jonas Valen

import java.util.Scanner;

public class Guess {
	public static void main(String[] args) {
		int n = 500;
		int min = 1;
		int max = 1000;
		String response = "";
		Scanner line = new Scanner(System.in);
		while (!response.equals("correct")) {
			System.out.println(n);
			response = line.nextLine();
			if (response.equals("lower")) {
				max = n-1;
				n = max - ((max-min)/2);
			} else if (response.equals("higher")){
				min = n+1;
				n = max - ((max-min)/2);
			}
		}
		line.close();
	}
}
