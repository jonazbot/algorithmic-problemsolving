package kattis.solutions;


import kattis.library.DisjointSets;
import kattis.library.Kattio;

import java.util.*;

public class LandLine implements Comparator<int[]> {

    private final Kattio io;
    private final int n, m, p;
    private final DisjointSets<Integer> sets;
    private final PriorityQueue<int[]> pq;
    private final Set<Integer> insecure, added;
    private final List<int[]> mst;

    /**
     * The mayor wants only communications intended for these insecure buildings to reach them.
     * In other words,  no communication from any building A to any building B should pass through
     * any insecure building C in the network (where C is different from A and B).
     */
    public LandLine() {
        this.io = new Kattio(System.in);
        this.n = io.getInt(); // [1, 1000] number of buildings.
        this.m = io.getInt(); // [0, 100,000] number of possible direct connections between a pair of buildings.
        this.p = io.getInt(); // [0, n] the number of insecure buildings

        if(n == 1) {System.out.println(0); System.exit(0); }

        this.insecure = new HashSet<>(p); // Must be leaves!
        this.added = new HashSet<>(p);
        this.sets = new DisjointSets<>(n);
        this.mst = new ArrayList<>(n);
        this.pq = new PriorityQueue<>(m, this);

        getInput();
        findMST();
        output();
    }

    private void getInput() {
        for (int i = 0; i < p; ++i) { insecure.add(io.getInt()); }
        for (int i = 0; i < m; ++i) {
            int[] edge = new int[3];
            edge[0] = io.getInt(); // x
            edge[1] = io.getInt(); // y
            edge[2] = io.getInt(); // weight
            pq.offer(edge);
        }
    }

    private void findMST() {
        for (int i = 1; i <= n; ++i) { sets.addOne(i); }
        while (mst.size() < n-1 && !pq.isEmpty()) {
            int[] edge = pq.poll();
            if (edge == null) { break; }
            if (insecure.contains(edge[0]) && insecure.contains(edge[1]) && n != 2) { System.err.println("Skipping "+Arrays.toString(edge)); continue; }
            if (!added.contains(edge[0]) && !added.contains(edge[1]) && !sets.find(edge[0]).equals(sets.find(edge[1]))) {
                if (insecure.contains(edge[0])) { added.add(edge[0]); }
                if (insecure.contains(edge[1])) { added.add(edge[1]); }
                sets.union(edge[0], edge[1]);
                mst.add(edge);
            }
        }
    }

    private void output() {
        int sum = 0;
        if (mst.size() == n - 1) {
            for (int[] edge : mst) { sum += edge[2]; }
            System.out.println(sum);
        }
        else System.out.println("impossible");
    }

    @Override
    public int compare(int[] a, int[] b) {
        if (a[2] < b[2]) return -1;
        else if (a[2] > b[2]) return 1;
        return 0;
    }

    public static void main(String[] args) { new LandLine(); }
}
