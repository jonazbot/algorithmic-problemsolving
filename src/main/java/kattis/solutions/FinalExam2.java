package kattis.solutions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FinalExam2 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int questions = Integer.parseInt(br.readLine());
        int score = 0;
        char prev, next;
        prev = br.readLine().charAt(0);
        for (int n = 1; n != questions; ++n) {
            next = br.readLine().charAt(0);
            if (prev == next) { ++score; }
            prev = next;
        }
        System.out.print(score);
    }
}
