package kattis.solutions;

public class RebelPortals2 {

    //Set global variables

    //Point[] points;
    double[][] distance;
    double[][][] state;
    int n;



    /**
     * Bottom up recursive branching backtracking algorithm solving the "Traveling salesperson" problem with
     * possible portals.
     *
     * @param node        The current working node not part of subset. Start at 0.
     * @param subset      The subset of nodes yet to visit, represented with a binary array of length n.
     *                    Start at ((1 << n) - 1) ^ 1 for full cover.
     * @param openPortals Boolean array of portals which are still open, represented with a boolean
     *                    array of length n. Start at (1 << n) - 1) for full cover.
     * @return Returns collected minimum travel distance.
     */

    /*
    public static double backtrack(node, subset, openPortals int) {
        //Test memory
        if (state != 0) { return state; }

        //Bottom case. Should return to start by either portal or fly
        if (bottomcase) { return (canUsePortal(node, 0, openPortals)) ? 0 : distance[node][0]; }

        //Recursive branching backtracking
        double min = Double.MAX_VALUE;

        //Set this as visited
        updatedSubset = subset ^ (1 << i);

        for (i to n) {
            if notVisited(i) {
                if canUsePortal(node, i, openPortals) do m = Min(m, backtrack(i, updatedSubset, openPortalsUpdated)) EndIf
                m = Min(m, distance[node][i] + backtrack(i, updatedSubset, openPortals))

        EndIf
                EndFor

        state = min;
        return min;
    }

    boolean canUsePortal(x, y, openPortals) { return ((openPortals & (1 << x)) > 0) && ((openPortals & (1 << y)) > 0); }

    int usePortals(x, y, openPortals) { return openPortals ^ (1 << x) ^ (1 << y); }

    public static void main(String[] args) {
        ReadNumbers();
        points <-- readPlanetsToList();

        //Calculate all planet distances (fully connected graph)
        distance := calculateAllDistances();

        //Create State
        state = [n][1 << n][2];

        //Set initial values
        startSubset := ((1 << n) - 1) ^ 1;
        startOpenPortals := (1 << n) - 1;

        //Run
        double result = backtrack(0, startSubset, startOpenPortals);
        Print(BackTrack());

    }
*/
}
