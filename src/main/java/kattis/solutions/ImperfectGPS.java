package kattis.solutions;

import kattis.library.Kattio;

public class ImperfectGPS {

    public ImperfectGPS() {
        Kattio io = new Kattio(System.in, System.out);
        int positions = io.getInt();
        int interval = io.getInt();

        double realDist = 0;
        double gpsDist = 0;
        double[] previous = new double[2];

        double[][] coordinates = new double[positions][3];
        for (int i = 0; i < positions; i++) {
            coordinates[i] = new double[] { io.getDouble(), io.getDouble(), io.getDouble() };
            if(i == 0) { previous = coordinates[i]; }
            if(i > 0) {
                double[] a = coordinates[i - 1];
                double[] b = coordinates[i];
                realDist += Math.hypot(a[0] - b[0], a[1] - b[1]);
                double start = a[2];
                double end = b[2];
                for (int j = (int) start; j <= end; ++j) {
                    if (j % interval == 0) {
                        gpsDist += Math.hypot(
                                a[0] + (b[0] - a[0]) * (j - start) / (end - start) - previous[0],
                                a[1] + (b[1] - a[1]) * (j - start) / (end - start) - previous[1]
                        );
                        previous[0] = a[0] + (b[0] - a[0]) * (j - start) / (end - start);
                        previous[1] = a[1] + (b[1] - a[1]) * (j - start) / (end - start);
                        previous[2] = j;
                    }
                }
            }
        }
        gpsDist += Math.hypot(coordinates[coordinates.length - 1][0] - previous[0], coordinates[coordinates.length - 1][1] - previous[1]);
        io.print((1 - gpsDist / realDist) * 100);
        io.close();
    }


    public static void main(String[] args) { new ImperfectGPS(); }
}