package kattis.solutions;

import kattis.library.Kattio;

import java.util.Arrays;
import java.util.HashSet;


public class CardTrading {
    public static void main(String[] args) {
        Kattio io = new Kattio(System.in);
        int n = io.getInt();
        int t = io.getInt();
        int k = io.getInt();
        HashSet<Integer> two = new HashSet<>();
        HashSet<Integer> one = new HashSet<>();

        int card;
        for (int i = 0; i<n; ++i) {
            card = io.getInt() -1;
            if (!one.add(card)) { one.remove(card); two.add(card); }
        }
        long deckValue = 0;
        long[] cost = new long[t];
        for (int i = 0; i < t; ++i) {
            if (two.contains(i)) {
                io.getLong();
                cost[i] = 2 * io.getLong();
                deckValue += cost[i];
            }
            else if (one.contains(i)) {
                long buy = io.getLong();
                long sell = io.getLong();
                cost[i] = buy + sell;
                deckValue += sell;
            }
            else {
                cost[i] = 2 * io.getLong();
                io.getLong();
            }
        }
        Arrays.sort(cost);
        for (int i = 0; i < k; ++i) { deckValue -= cost[i]; }
        System.out.print(deckValue);


    }
}
