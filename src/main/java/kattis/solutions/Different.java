package kattis.solutions;

import kattis.library.Kattio;

public class Different {

    public static void main(String[] args) {
        Kattio io = new Kattio(System.in, System.out);
        while (io.hasMoreTokens()) { io.println(Math.abs(io.getLong() - io.getLong())); }
        io.close();
    }
}
