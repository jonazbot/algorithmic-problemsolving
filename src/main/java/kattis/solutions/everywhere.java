
import java.util.*;
public class everywhere{
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		int cases = in.nextInt();
		for (int i = 0; i < cases; i++){
			int nCities = in.nextInt();
			in.nextLine();
			Set<String> citySet = new HashSet<>();
				for (int j = 0; j < nCities; j++){
					String city = in.nextLine();
					citySet.add(city);
				}
				System.out.println(citySet.size());
		} 
	}
}
