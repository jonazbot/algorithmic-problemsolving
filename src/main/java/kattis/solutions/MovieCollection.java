package kattis.solutions;

import kattis.library.Kattio;

public class MovieCollection {
    private final Kattio io;
    private int m, r;
    private int[] tree, stack, sequence;

    MovieCollection() {
        this.io = new Kattio(System.in, System.out);
        int cases = io.getInt();
        for (int caseNum = 0; caseNum < cases; ++caseNum) {
            getInput();
            for (int i = 1; i <= m; i++) {
                update(i, 1);
                stack[i] = m-i+1;
            }
            for (int i = 1; i <= r; ++i) { io.print(query(m+i, sequence[i-1]) + " "); }
            io.println();
        }
        io.close();
    }

    private int query(int query, int movie) {
        int index = stack[movie];
        int sum = 0;
        while (index > 0) {
            sum += tree[index];
            index -= index & (-index);
        }
        update(stack[movie], -1);
        stack[movie] = query;
        update(stack[movie], 1);
        return m - sum;
    }

    private void update(int index, int value) {
        if (index < tree.length) {
            tree[index] += value;
            index += -index & index;
            update(index, value);
        }
    }

    private void getInput() {
        m = io.getInt();
        r = io.getInt();
        stack = new int[m+1];
        tree = new int[m+r];
        sequence = new int[r];
        for (int i = 0; i < r; ++i) { sequence[i] = io.getInt(); }
    }

    public static void main(String[] args) { new MovieCollection(); }
}