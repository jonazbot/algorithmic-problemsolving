// Cold.java
// Cold-puter Science - open.kattis.com/problems/cold
// By Jonas Valen

import java.util.Scanner;
import java.util.Arrays;

public class Cold {
    public static void main(String[] args) {
        Scanner line = new Scanner(System.in);
        int n = line.nextInt();
        int coldDays = 0;
		for (int i = 0; i<n; i++){
			int s = line.nextInt();
			if(s<0){
				coldDays++;
			}
		} 
        System.out.println(coldDays);
        
    }
}
