package kattis.solutions;

import kattis.library.Kattio;

import java.util.ArrayList;

public class InquiryI {

    public static void main(String[] args) {
        Kattio io = new Kattio(System.in);
        long leftSum = 0;
        long rightSum = 0;
        int size = io.getInt();
        ArrayList<Integer> integers = new ArrayList<>(size);
        integers.add(io.getInt());
        leftSum += (long) integers.get(0) * integers.get(0);
        for (int i = 1; i < size; ++i) {
            integers.add(io.getInt());
            rightSum += integers.get(integers.size()-1);
        }
        long max = leftSum * rightSum;
        for (int i = 1; i < size-1;i++) {
            rightSum -= integers.get(i);
            leftSum += (long) integers.get(i) * integers.get(i);
            if (leftSum * rightSum > max) { max = leftSum * rightSum; }
        }
        System.out.print(max);
    }
}
