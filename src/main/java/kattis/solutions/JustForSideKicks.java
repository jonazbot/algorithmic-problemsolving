package kattis.solutions;

import kattis.library.Kattio;

public class JustForSideKicks {
    private final Kattio io;
    private final int n, q;
    private final long[] values;
    private final int[] gems;
    private final int[][] trees;

    JustForSideKicks() {
        this.io = new Kattio(System.in, System.out);
        this.n = io.getInt();
        this.q = io.getInt();
        this.values = new long[6];
        for (int i = 0; i < 6; ++i) { values[i] = io.getLong(); }
        this.gems = new int[n];
        char[] chars = io.getWord().toCharArray();
        for (int i = 0; i < n; ++i) { gems[i] = chars[i] - 49; }
        this.trees = new int[6][n+1];
        for (int i = 0; i < gems.length; ++i) { update(trees[gems[i]], i + 1, 1); }
        for (int i = 0; i < q; ++i) { query(); }
        io.close();
    }

    private void update(int[] tree, int index, long value) {
        while (index <= n) {
            tree[index] += value;
            index += -index & index;
        }
    }

    private void replaceGem(int index, int newGem) {
        int oldGem = gems[index-1];
        gems[index-1] = --newGem;
        update(trees[oldGem], index, -1);
        update(trees[newGem], index, 1);
    }

    private void setValue(int p, int value) { values[--p] = value; }

    private void query() {
        int queryType = io.getInt();
        int a = io.getInt();
        int b = io.getInt();
        if (queryType == 1) { replaceGem(a, b); }
        else if (queryType == 2) { setValue(a, b); }
        else if (queryType == 3) { io.println(rangeQuery(a, b)); }
    }

    private long rangeQuery(int lIndex, int rIndex) {
        long sum = 0;
        for (int i = 0; i < 6; ++i) { sum += values[i] * (findPrefixSum(trees[i], rIndex) - findPrefixSum(trees[i],lIndex-1)); }
        return sum;
    }

    private long findPrefixSum(int[] tree, int index) {
        long sum = 0;
        while(index > 0) {
            sum += tree[index];
            index -= -index & index;
        }
        return sum;
    }

    public static void main(String[] args) { new JustForSideKicks(); }
}
