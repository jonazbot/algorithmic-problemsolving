package kattis.solutions;

import kattis.library.DisjointSets;
import kattis.library.Kattio;

public class BridgesAndTunnels {
    private Kattio io;
    private final int n;
    private final DisjointSets<String> sets;

    BridgesAndTunnels() {
        this.io = new Kattio(System.in);
        this.n = io.getInt();
        this.sets = new DisjointSets<>(n);
        getInput(n);
        io.close();
    }

    private void getInput(int n) {
        for (int i = 0; i < n; ++i) {
            String buildingA = io.getWord();
            String buildingB = io.getWord();
            sets.add(buildingA, buildingB);
            io.println(sets.getSizes().get(sets.find(buildingA)));
        }
    }

    public static void main(String[] args) { new BridgesAndTunnels(); }
}
