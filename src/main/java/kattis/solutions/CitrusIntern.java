package kattis.solutions;

import kattis.library.Kattio;

import java.util.List;

public class CitrusIntern {
    private final int[][] orgTree;
    private final long[][] memo;
    private final long[] bribe;
    private final int[] bosses;

    /**
     * Constructor for Kattis problem 'Citrus Intern'
     */
    public CitrusIntern() {
        final Kattio io = new Kattio(System.in);
        int nodes = io.getInt();
        this.orgTree = new int[nodes][];
        this.memo = new long[nodes][3];
        this.bribe = new long[nodes];
        this.bosses = new int[nodes];

        for (int i = 0; i < nodes; ++i) {
            this.bribe[i] = io.getInt();
            int n = io.getInt();
            if (n == 0) {   // Leaves are the base-case.
                memo[i][0] = bribe[i];
                memo[i][1] = 0L;
                memo[i][2] = Long.MAX_VALUE;
            }
            this.orgTree[i] = new int[n];
            for (int j = 0; j < n; ++j) {
                int child = io.getInt();
                this.orgTree[i][j] = child;
                this.bosses[child] = i + 1;  // Add 1 to ensure only root can be 0.
            }
        }
        System.out.println(infiltrate(findRoot(0)));
    }

    /**
     * Constructor for JUnit testing.
     *
     * @param nodes number of nodes in graph.
     * @param input n lines of integers.
     */
    public CitrusIntern(int nodes, List<List<Integer>> input) {
        this.orgTree = new int[nodes][3];
        this.memo = new long[nodes][3];
        this.bosses = new int[nodes];
        this.bribe = new long[nodes];

        for (int i = 0; i < nodes; ++i) {
            this.bribe[i] = input.get(i).get(0);
            int n = input.get(i).get(1);
            if (n == 0) {   // Leaves are the base-case.
                memo[i][0] = bribe[i];
                memo[i][1] = 0L;
                memo[i][2] = Long.MAX_VALUE;
            }
            this.orgTree[i] = new int[n];
            for (int j = 0; j < n; ++j) {
                int child = input.get(i).get(j+2);
                this.orgTree[i][j] = child;
                this.bosses[child] = i + 1; // Add 1 to ensure only root can be 0.
            }
        }
    }

    /**
     * Infiltrate Citrus,
     *
     * @param root The 'one and only most sour excellence'.
     * @return The minimum cost of infiltrating the entire Citrus organization.
     */
    public long infiltrate(int root) {
        solve(root);
        return Long.min(this.memo[root][0], this.memo[root][2]);
    }

    /**
     * Find the root of the tree by going from node to parent recursively until no parent is found.
     * Adds 1 to all bosses so only root can be initial value 0.
     *
     * @param node The node to start traversing to find root.
     * @return The root node.
     */
    private int findRoot(int node) {
        if (bosses[node] == 0) { return node; } // Only root can be 0.
        return findRoot(bosses[node] - 1); // Decipher the + 1 root 'encryption'.
    }

    /**
     * Solves recursively from the leaves to the root.
     *
     * @param root The root of the search.
     */
    private void solve(int root) {
        for (int child : this.orgTree[root]) { solve(child); }
        this.memo[root][0] = pickNode(root);
        this.memo[root][1] = pickParent(root);
        this.memo[root][2] = pickChild(root);
    }

    /**
     * Cost of picking a node.
     *
     * @param node The node to pick.
     * @return The cost to pick this node.
     */
    private long pickNode(int node) {
        long sum = 0L;
        for (int child: orgTree[node]) { sum += memo[child][1]; }
        return bribe[node] + sum;
    }

    /**
     * Cost of skipping node and picking the parent.
     *
     * @param node The node to skip.
     * @return The cost of picking the parent.
     */
    private long pickParent(int node) {
        long sum = 0L;
        for (int child: orgTree[node]) { sum += Long.min(memo[child][0], memo[child][2]); }
        return sum;
    }

    /**
     * Cost of skipping node and picking a child.
     *
     * @param node The node to skip.
     * @return The cost of picking a child.
     */
    private long pickChild(int node) {
        long min = Long.MAX_VALUE;
        for (int child: orgTree[node]) {
            long promote = Long.max(memo[child][0] - memo[child][2],0);  // Cost of picking rather than skipping.
            if (promote < min) { min = promote; }
        }
        return memo[node][1] + min;
    }

    public static void main(String[] args) { new CitrusIntern(); }
}
