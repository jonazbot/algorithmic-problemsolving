package kattis.solutions;

import kattis.library.Kattio;

import java.util.ArrayList;
import java.util.List;

/**
 * Find an ordering of planets (nodes) that minimizes distance.
 *
 * NP-Complete.
 * Target Time Complexity: O(n^2 * 2^n)
 * Node 0 is start and end.
 * Path != Reverse Path.
 * Complete graph.
 * Hamiltonian cycle of the least weight.
 * DP:  BaseCase = Distance from 0 to all nodes.
 *      Step = Distance from 0 to all nodes to all remaining nodes (DP+1 visited)
 *
 * I'M A DYSLECTIC IDIOT WHO CAN'T READ PROBLEM:
 *      Can backtrack to use TP... FML
 *      TP part way: Only add Dist to visited but unused TP.
 *      TP state must be represented along with visited state
 *      Choices: TP, NoTP/Fly, SkipTP/Fly (can be used later)...
 *          Does this still mean there is never a good reason to skip 3?
 *
 */
public class RebelPortals {
    private final int nodes;
    private double minTravel;
    private final int[][] locs;
    private final double[][] dist;
    private final Double[][] tpMemo;      //  Double object to set values to null.
    private final Double[][] flyMemo;     //  Double object to set values to null.
    private final Double[][] fly2Memo;    //  Double object to set values to null.

    private final Double[][] memo;        //  Double object to set values to null.
    private final Integer[][] tpStates;   //  Double object to set values to null.

    /**
     * Constructor used in Kattis.
     */
    public RebelPortals() {
        Kattio io = new Kattio(System.in);
        this.nodes = io.getInt();
        this.locs = new int[this.nodes][3];
        this.dist = new double[this.nodes][this.nodes];
        this.minTravel = Double.MAX_VALUE;
        this.tpMemo = new Double[nodes][1 << nodes];    // n*2^n matrix
        this.flyMemo = new Double[nodes][1 << nodes];   // n*2^n matrix
        this.fly2Memo = new Double[nodes][1 << nodes];   // n*2^n matrix

        this.memo = new Double[nodes][1 << nodes];   // n*2^n matrix
        this.tpStates = new Integer[nodes][1 << nodes];   // n*2^n matrix

        for (int l = 0; l < nodes; ++l) { for (int i = 0; i < 3; ++i) { locs[l][i] = io.getInt(); } }
        findDistances();
        dynamic();
        returnHome();
        System.out.println(this.minTravel);
    }

    /**
     * Constructor for JUnit testing.
     *
     * @param nodes The number of nodes.
     * @param input The input.
     */
    public RebelPortals(int nodes, List<List<Integer>> input) {
        this.nodes = nodes;
        this.locs = new int[nodes][3];
        this.dist = new double[nodes][nodes];
        this.minTravel = Double.MAX_VALUE;
        this.tpMemo = new Double[nodes][1 << nodes];    // Why did the program slow down with combined memo[n][2^n][3]?
        this.flyMemo = new Double[nodes][1 << nodes];
        this.fly2Memo = new Double[nodes][1 << nodes];

        this.memo = new Double[nodes][1 << nodes];
        this.tpStates = new Integer[nodes][1 << nodes];   // n*2^n matrix

        for (int l = 0; l < nodes; ++l) {
            for (int i = 0; i < 3; ++i) { this.locs[l][i] = input.get(l).get(i); } }
        findDistances();
        dynamic();
        returnHome();
        System.out.println("Least weighted Hamiltonian cycle: "+this.minTravel);
    }

    private void dynamic() {
        // Base case
        for (int i = 1; i < this.nodes; ++i) {
            this.tpMemo[i][1 | 1 << i] = 0.0;
            this.flyMemo[i][1 | 1 << i] = this.dist[0][i];
            this.fly2Memo[i][1 | 1 << i] = this.dist[0][i];

            this.tpStates[i][1 | 1 << i] = 1 | 1 << i;
            this.memo[i][1 | 1 << i] = this.dist[0][i];
        }
        // Recurrence
        for (int r = 3; r <= this.nodes; ++r) {
            for (int subset : subSetGenerator(r)) {
                if (notIn(0, subset)) { continue; }       // Start node 0 must be visited to be a valid subset.
                for (int next = 1; next < this.nodes; ++next) {
                    if (notIn(next, subset)) { continue; }      // next must be visited to be a valid subset.
                    branch(next, subset);
                }
            }
        }
    }

    private void branch(int next, int subset) {
        double minDistMustFly = Double.MAX_VALUE;
        double minDistFlyTwice = Double.MAX_VALUE;
        for (int prev = 1; prev < this.nodes; ++prev) {
            if (prev == next || notIn(prev, subset)) { continue; }  // prev must be visited prev be a valid subset.
            int preVisit = subset ^ (1 << next);        //preVisit = subset before visiting next.
            double fly = this.tpMemo[prev][preVisit] + this.dist[prev][next];
            if (fly < minDistMustFly) { minDistMustFly = fly; }
            double flyTwice = this.flyMemo[prev][preVisit] + this.dist[prev][next];
            if (flyTwice < minDistFlyTwice) { minDistFlyTwice = flyTwice; }
            this.tpMemo[next][subset] = this.flyMemo[prev][preVisit];
            this.flyMemo[next][subset] = minDistMustFly;
            this.fly2Memo[next][subset] = flyTwice;
            this.tpStates[next][subset] = tpStates[prev][preVisit]; // TODO: Shift Bit AND TEST! =|(1 << n) | num
        }

    }

    private void returnHome() {
        int endState = (1 << this.nodes) - 1;
        for (int end = 1; end < this.nodes; ++end) {
            double pathCostTPHome = this.flyMemo[end][endState];
            double pathCostFly2Home = this.fly2Memo[end][endState];
            double pathCostFlyHome = this.tpMemo[end][endState] + this.dist[0][end];
            double pathCost = Double.min(pathCostFlyHome, pathCostTPHome);
            if (pathCost < this.minTravel) { this.minTravel = pathCost; }
        }
    }

    /**
     * Check if the bit at index in bitSet is 0.
     *
     * @param index The index in the bit-set.
     * @param subset The bit-set.
     * @return true if the bit is 0, false otherwise.
     */
    private boolean notIn(int index, int subset) { return ((1 << index) & subset) == 0; }

    /**
     * Generate bitset permutations of rate nodes visited.
     *      TODO: Can do nodes - 1 and eliminate 0 in subset.
     *
     * @param rate The number of 1s.
     *
     * @return Every bit-set of length node that has rate number of bits set to 1.
     */
    private List<Integer> subSetGenerator(int rate) {
        List<Integer> subsets = new ArrayList<>(1 << (nodes-2));    // 2^n rather than nCr.
        subSetGenerator(0, 0, rate, subsets);
        return subsets;
    }

    private void subSetGenerator(int set, int at, int rate, List<Integer> subsets) {
        if (rate == 0) { subsets.add(set);}
        else {
            for (int i = at; i < this.nodes; ++i) {
                set |= (1 << i);
                subSetGenerator(set, i + 1, rate - 1, subsets);
                set &= ~(1 << i);
            }
        }
    }
    /**
     * Find the distance between all points.
     */
    private void findDistances() {
        for (int a = 0; a < nodes; ++a) {
            for (int b = a + 1; b < nodes; ++b) {
                dist[a][b] = distance(locs[a], locs[b]);
                dist[b][a] = dist[a][b];
            }
        }
    }

    /**
     * Find the Euclidean distance between two points in 3-dimensional space.
     *
     * @param a Point a.
     * @param b Point b.
     * @return The distance from point a to  point b
     */
    private double distance(int[] a, int[] b) {
        return Math.sqrt(Math.pow(a[0] - b[0], 2) + Math.pow(a[1] - b[1], 2) + Math.pow(a[2] - b[2], 2)); }

    public double getMinTravel() { return this.minTravel; }

    public static void main(String[] args) { new RebelPortals(); }
}