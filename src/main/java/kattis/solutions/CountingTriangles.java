package kattis.solutions;

import kattis.library.Kattio;

public class CountingTriangles {

    public CountingTriangles() {
        Kattio io = new Kattio(System.in, System.out);
        while (true) {
            int n = io.getInt();
            if (n == 0) { break; }

            double[][] lines = new double[n][4];
            for (int i = 0; i < n; i++) {
                lines[i][0] = io.getDouble();
                lines[i][1] = io.getDouble();
                lines[i][2] = io.getDouble();
                lines[i][3] = io.getDouble();
            }

            int total = 0;
            for (int a = 0; a < n; ++a) {
                for (int b = a+1; b < n; ++b) {
                    if (linesIntersect(lines[a], lines[b])) {
                        for (int c = b+1; c < n; ++c) {
                            if (linesIntersect(lines[b], lines[c]) && linesIntersect(lines[a], lines[c])) { ++total; }
                        }
                    }
                }
            }
            io.println(total);
        }
        io.close();
    }

    public boolean linesIntersect(double[] lineA, double[] lineB) {
        // ((Ax2 - Ax1) * (By2 - By1)) - ((Ay2 - Ay1) * (Bx2 - Bx1))
        double d = ((lineA[2] - lineA[0]) * (lineB[3] - lineB[1])) - ((lineA[3] - lineA[1]) * (lineB[2] - lineB[0]));
        if (d == 0) return false;

        // (((Ay1 - By1) * (Bx2 - Bx1)) - ((Ax1 - Bx1) * (By2 - By1))) / d
        double intersectionA = (((lineA[1] - lineB[1]) * (lineB[2] - lineB[0])) - ((lineA[0] - lineB[0]) * (lineB[3] - lineB[1]))) / d;
        // (((Ay1 - By1) * (Ax2 - Ax1)) - ((Ax1 - Bx1) * (Ay2 - Ay1))) / d
        double intersectionB = (((lineA[1] - lineB[1]) * (lineA[2] - lineA[0])) - ((lineA[0] - lineB[0]) * (lineA[3] - lineA[1]))) / d;

        return (intersectionA >= 0 && intersectionA <= 1) && (intersectionB >= 0 && intersectionB <= 1);
    }


    public static void main(String[] args) { new CountingTriangles(); }

}