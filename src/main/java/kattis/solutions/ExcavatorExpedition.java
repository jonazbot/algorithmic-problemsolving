package kattis.solutions;

import kattis.library.Kattio;

import java.util.ArrayList;

public class ExcavatorExpedition {
    private final int v, e;
    private final int[] sites, funLevels;
    private final ArrayList<Integer>[] children;
    private final Kattio io;

    ExcavatorExpedition(){
        io = new Kattio(System.in, System.out);
        v = io.getInt();
        e = io.getInt();
        sites = new int[v];
        funLevels = new int[v];
        children = new ArrayList[v];
        getInput();
        io.print(findMaxFun());
        io.close();
    }

    private void getInput() {
        String str = io.getWord();
        for (int i = 0; i < v; ++i) {
            sites[i] = str.charAt(i) == 'X' ? 1: -1;
            children[i] = new ArrayList<>();
        }
        sites[0] = 0;
        for (int i = 0; i < e; ++i) { children[io.getInt()].add(io.getInt()); }
    }

    private int findMaxFun() {
        boolean updated = true;
        while (updated) {
            updated = false;
            for (int i = v-2; i >= 0; i--) {
                int maxChild = -v;
                for (int node : children[i]) { if (funLevels[node] > maxChild) { maxChild = funLevels[node]; } }
                if (sites[i] + maxChild != funLevels[i]) {
                    funLevels[i] = sites[i] + maxChild;
                    updated = true;
                }
            }
        }
        return funLevels[0];
    }

    public static void main(String[] args) { new ExcavatorExpedition(); }
}