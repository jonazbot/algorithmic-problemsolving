import java.util.*;

public class HissingMicrophone {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String line = in.nextLine();
		char[] chars = line.toCharArray();
		boolean hiss = false;
		for (int i = 1; i < line.length(); i++) {
			if (chars[i] == 's' && chars[i-1] == 's') {
				hiss = true;
			}
		}
		if(hiss) {
			System.out.println("hiss");
		}
		else {
			System.out.println("no hiss");
		}
		
	}
}
