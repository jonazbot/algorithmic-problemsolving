package kattis.solutions;

import kattis.library.Kattio;

import java.util.ArrayList;
import java.util.HashSet;

public class MoneyMatters {
    private final int[] bill;
    private final ArrayList<ArrayList<Integer>> friendships;
    private final HashSet<Integer> visited;

    public MoneyMatters(int numFriends) {
        this.bill = new int[numFriends];
        this.friendships = new ArrayList<>(numFriends);
        for (int i = 0; i < numFriends; i++) { friendships.add(new ArrayList<>()); }
        this.visited = new HashSet<>(numFriends);
    }

    public void addBill(int person, int amount) { this.bill[person] = amount; }

    public void addFriendship(int friend1, int friend2) {
        friendships.get(friend1).add(friend2);
        friendships.get(friend2).add(friend1);
    }

    private int pay(int person, int sum) {
        visited.add(person);
        sum += bill[person];
        for (int friend : friendships.get(person)) { if (!visited.contains(friend)) { sum = pay(friend, sum); } }
        return sum;
    }

    public String solve() {
        for (int friend = 0; friend < friendships.size(); friend++) {
            if (!visited.contains(friend)) { if (pay(friend, 0) != 0) { return "IMPOSSIBLE"; } }
        }
        return "POSSIBLE";
    }

    public static void main(String[] args) {
        Kattio io = new Kattio(System.in);
        int friends = io.getInt();
        int friendships = io.getInt();
        MoneyMatters mm = new MoneyMatters(friends);
        for (int friend = 0; friend < friends; ++friend) { mm.addBill(friend, io.getInt()); }
        for (int friendship = 0; friendship < friendships; ++friendship) { mm.addFriendship(io.getInt(), io.getInt()); }
        System.out.print(mm.solve());
    }

}

