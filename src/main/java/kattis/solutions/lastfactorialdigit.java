//lastfactorialdigit.java
//By Jonas Valen
import java.util.*;
public class lastfactorialdigit{
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		for (int i = 0;i<n;i++){
			int number = in.nextInt();
			long factor = factorial(number);
			String lastdigit = String.valueOf(factor);
			System.out.println(lastdigit.substring(lastdigit.length() -1));
		}
	}
	public static long factorial(int n){
		long result = 1;
		for (int i = 1;i <= n; i++){
			result = result * i;
		}
		return result;
	}
}
