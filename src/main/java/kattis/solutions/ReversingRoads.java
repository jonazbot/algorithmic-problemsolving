package kattis.solutions;

import kattis.library.Kattio;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

public class ReversingRoads {

    private final HashMap<Integer, ArrayList<Integer>> map;
    private final HashMap<Integer, ArrayList<Integer>> revMap;
    private final ArrayList<int[]> inputList;
    private final int nodes;

    public ReversingRoads(int numNodes) {
        this.nodes = numNodes;
        this.map = new HashMap<>(nodes);
        this.revMap = new HashMap<>(nodes);
        this.inputList = new ArrayList<>();
    }
    public void addInput(int from, int to) {
        this.inputList.add(new int[] {from, to});
        addRoad(from, to);
    }

    private void addRoad(int from, int to) {
        this.map.computeIfAbsent(from, value -> new ArrayList<>());
        this.map.get(from).add(to);
        this.revMap.computeIfAbsent(to, value -> new ArrayList<>());
        this.revMap.get(to).add(from);
    }

    private boolean reverseTest(int from, int to) {
        int indexTo = this.map.get(from).indexOf(to);
        this.map.get(from).remove(indexTo);
        int indexFrom = this.revMap.get(to).indexOf(from);
        this.revMap.get(to).remove(indexFrom);
        addRoad(to, from);

        if (DFS(this.map).size() == this.nodes && DFS(this.revMap).size() == this.nodes) { return true; }

        if (this.map.get(to).size() == 1) { this.map.remove(to); }
        else { this.map.get(to).remove(Integer.valueOf(from)); }
        if (this.revMap.get(from).size() == 1) { this.revMap.remove(from); }
        else { this.revMap.get(from).remove(Integer.valueOf(to)); }

        this.map.get(from).add(indexTo, to);
        this.revMap.get(to).add(indexFrom, from);

        return false;
    }

    public String checkRoads() {
        if (this.map.size() >= this.nodes -1 && this.revMap.size() >= this.nodes -1) {
            if (this.map.size() == this.nodes && this.revMap.size() == this.nodes) {
                if (DFS(this.map).size() == this.nodes && DFS(this.revMap).size() == this.nodes) { return "valid"; }
                else {


                    for (int[] input : inputList) {
                        if (map.get(input[0]).size() > 1 && revMap.get(input[1]).size() > 1
                                && reverseTest(input[0], input[1])) {
                            return String.format("%d %d", input[0], input[1]);
                        }
                    }
                }
            }
            else {
                HashSet<Integer> intersect = new HashSet<>(this.map.keySet());
                intersect.removeAll(this.revMap.keySet());
                HashSet<Integer> revIntersect = new HashSet<>(this.revMap.keySet());
                revIntersect.removeAll(this.map.keySet());

                if (intersect.size() == 1 && revIntersect.size() == 1) {
                    int from = intersect.iterator().next();
                    int to = revIntersect.iterator().next();
                    if (this.map.containsKey(from) && this.map.get(from).contains(to)
                            && reverseTest(from, to)) {
                        return String.format("%d %d", from, to);
                    }
                }
                else if (intersect.size() == 1 && revIntersect.size() == 0) {
                    int from = intersect.iterator().next();
                    for (int index = 0; index < this.map.get(from).size(); ++index) {
                        int to = this.map.get(from).get(index);
                        if (this.revMap.get(to).size() > 1 && reverseTest(from, to)) {
                            return String.format("%d %d", from, to);
                        }
                    }
                }
                else if (intersect.size() == 0 && revIntersect.size() == 1) {
                    int to = revIntersect.iterator().next();
                    for (int index = 0; index < this.revMap.get(to).size(); ++index) {
                        int from = this.revMap.get(to).get(index);
                        if (this.map.get(from).size() > 1 && reverseTest(from, to)) {
                            return String.format("%d %d", from, to);
                        }
                    }
                }
            }
        }
        return "invalid";
    }

    private HashSet<Integer> DFS(HashMap<Integer, ArrayList<Integer>> graph) {
        HashSet<Integer> visited = new HashSet<>(this.nodes);
        Stack<Integer> stack = new Stack<>();
        stack.add(inputList.get(0)[0]);
        while (!stack.isEmpty()) {
            int current = stack.pop();
            visited.add(current);
            for (int node : graph.get(current)) { if (!visited.contains(node)) { stack.add(node); } }
        }
        return visited;
    }

    public static void main(String[] args) {
        Kattio io = new Kattio(System.in, System.out);
        int caseNum = 1;
        while (io.hasMoreTokens()) {
            int numNodes = io.getInt();
            int numRoads = io.getInt();
            if (numNodes == 1) { io.println(String.format("Case %d: valid", caseNum)); }
            else if (numNodes > numRoads) {
                for (int i = 0; i < numRoads; ++i) { io.getInt(); io.getInt(); }
                io.println(String.format("Case %d: invalid", caseNum));
            }
            else {
                ReversingRoads roads = new ReversingRoads(numNodes);
                for (int i = 0; i < numRoads; ++i) { roads.addInput(io.getInt(), io.getInt()); }
                io.println(String.format("Case %d: %s", caseNum, roads.checkRoads()));
            }
            ++caseNum;
        }
        io.close();
    }

}

