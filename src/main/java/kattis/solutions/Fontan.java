package kattis.solutions;

import kattis.library.Coordinate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Fontan {
    private final int n;
    private final int m;
    private final char[][] grid;
    private final BufferedReader br;
    private final Queue<Coordinate> queue;

    public Fontan() throws IOException {
        this.queue = new LinkedList<>();
        this.br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer tokens = new StringTokenizer(br.readLine(), " ");
        this.n = Integer.parseInt(tokens.nextToken());
        this.m = Integer.parseInt(tokens.nextToken());
        this.grid = new char[this.n][this.m];

        for (int y = 0; y != this.n; ++y) {
            grid[y] = br.readLine().toCharArray();
            for (int x = 0; x != this.m; ++x) { if (this.grid[y][x] == 'V') { this.queue.add(new Coordinate(x, y)); } }
        }
    }

    public void runWater() {
        while (!this.queue.isEmpty()) {
            Coordinate coord = this.queue.poll();
            if (inBounds(coord.getX(), coord.getY() +1)) {
                if (this.grid[coord.getY() +1][coord.getX()] == '.') {
                    this.grid[coord.getY() +1][coord.getX()] = 'V';
                    this.queue.add(new Coordinate(coord.getX(),coord.getY() +1));
                }
                else if (this.grid[(coord.getY() +1)][coord.getX()] == '#') {
                    if (inBounds(coord.getX() -1, coord.getY()) && this.grid[coord.getY()][coord.getX() -1] == '.') {
                        this.grid[coord.getY()][coord.getX() -1] = 'V';
                        this.queue.add(new Coordinate(coord.getX() -1, coord.getY()));
                    }
                    if (inBounds(coord.getX() +1, coord.getY()) && this.grid[coord.getY()][coord.getX() +1] == '.') {
                        this.grid[coord.getY()][coord.getX() +1] = 'V';
                        this.queue.add(new Coordinate(coord.getX() +1, coord.getY()));
                    }
                }
            }
        }
    }

    private boolean inBounds(int x, int y) { return x < this.m && x >= 0 && y < this.n; }

    public void printGrid() { for (char[] chars : this.grid) { System.out.println(String.valueOf(chars)); } }

    public static void main(String[] args) throws IOException {
        Fontan fontan = new Fontan();
        fontan.runWater();
        fontan.printGrid();
    }
}