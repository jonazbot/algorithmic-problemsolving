package kattis.solutions;

import kattis.library.Kattio;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Countdown {
    private final Deque<Integer> numbers;
    private int target;
    private int best;
    private final Map<String, Integer> results;

    public Countdown() {
        Kattio io = new Kattio(System.in, System.out);
        int cases = io.getInt();
        numbers = new LinkedList<>();
        results = new HashMap<>();
        best = 0;
        for (int c = 0; c < cases; ++c) {
            for (int i = 0; i < 6; ++i) { numbers.push(io.getInt()); }
            target = io.getInt();

            solve(numbers.pop(), numbers);

        }
    }

    public void solve(int num, Deque<Integer> deque) {
        better(num);
        if (num == target) { return; }
        for (int a = 0; a < deque.size(); ++a) {
            int b = deque.pop();
            add(a, b, deque);
            sub(a, b, deque);
            mult(a, b, deque);
            div(a, b, deque);
            deque.offer(b);
        }
    }

    private void add(int a, int b, Deque<Integer> stack) {
        int sum = a + b;
        stack.add(sum);
        solve(sum, stack);
    }

    private int sub(int a, int b, Deque<Integer> stack) {
        if (!results.containsKey(a+" - "+b) && a - b > 0) { results.put(a+" - "+b,a - b); }
        return a - b;
    }

    private int mult(int a, int b, Deque<Integer> stack) {
        if (!results.containsKey(a+" * "+b)) { results.put(a+" * "+b,a * b); }
        return results.get(a+" * "+b);
    }

    private int div(int a, int b, Deque<Integer> stack) {
        if (!results.containsKey(a+" / "+b)) {
            if (a % b == 0) { results.put(a + " / " + b, a / b); }
            else return -1;
        }
        return results.get(a + " * " + b);
    }

    private void better(int num) {
        if(Math.abs(target - num) < Math.abs(target - best)) {
            best = num;
        }
    }


    public String output() { return"Target: "+target+"\n"+show()+"Best approx: "+best+"\n"; }

    private String show() {
        return "";
    }

    public static void main(String[] args) { new Countdown(); }
}
