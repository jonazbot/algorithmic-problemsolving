package kattis.solutions;

import kattis.library.Coordinate;
import kattis.library.Kattio;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class TexasSummers {

    private final Kattio io;
    private final Coordinate[] shadedAreas;
    private final Coordinate dorm;
    private final Coordinate school;
    private double maxSweat;

    public TexasSummers() {
        io = new Kattio(System.in);
        int n = io.getInt();
        shadedAreas = new Coordinate[n];
        for (int i = 0; i < n; ++i) {
            shadedAreas[i] = new Coordinate(io.getInt(), io.getInt());
        }
        dorm = new Coordinate(io.getInt(), io.getInt());
        school = new Coordinate(io.getInt(), io.getInt());

        maxSweat = edgeSweatWeight(dorm, school);

        List<Integer> path = leastSweatyPath();
        if (path.size() == 0) { System.out.println("-"); }
        else { for (int i = 0; i < path.size(); ++i) { System.out.println(path.get(i)); } }
    }

    private List<Integer> leastSweatyPath() {
        Deque<Integer> path = new LinkedList<>();
        Deque<Coordinate> stack = new LinkedList<>();

        for (Coordinate shade: shadedAreas) {
            dfs(dorm, 0);
        }
        return (List<Integer>) path;
    }

    private void dfs(Coordinate current, double sweatSum) {
        double max = edgeSweatWeight(current, school);
        if (max < maxSweat) { maxSweat = max; }
        for (Coordinate next : shadedAreas) {
            double edgeSweat = edgeSweatWeight(current, next);
            if (edgeSweat < max) { dfs(next, sweatSum + edgeSweat); }
        }

    }

    // pythagorasSweat()?
    private double edgeSweatWeight(Coordinate from, Coordinate to) {
        return Math.pow(from.getX() - to.getX(), 2) + Math.pow(from.getY() - to.getY(), 2);
    }

    public static void main(String[] args) { new TexasSummers(); }
    
}

