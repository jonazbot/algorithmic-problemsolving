package kattis.solutions;


import kattis.library.Kattio;

public class ConstrainedFreedomOfChoice {
    private final Kattio io;
    private int r, c;
    private char[][] map;
    private int[][][] memory;
    private int[][][] visitedEdge;

    public ConstrainedFreedomOfChoice() {
        io = new Kattio(System.in, System.out);
        getInput();
        io.close();
    }

    private void getInput() {
        while (true) {
            r = io.getInt();
            c = io.getInt();
            if (r == 0 && c != 0) {io.println(0); continue;}
            if (r != 0 && c == 0) {io.println(0); continue;}
            if (r == 0 && c == 0) break;
            getMap();
            visitedEdge = new int[r][c][3];
            visitedEdge[0][c-1][0] = 1;
            memory[0][c-1][0] = 1;
            dfs(r-1, 0, 0);
            io.println((memory[r-1][0][0]+memory[r-1][0][1]));
        }
    }

    private void getMap() {
        map = new char[r][c];
        memory = new int[r][c][3];
        memory[0][c-1][0] = 1;
        for (int y = 0; y < r; ++y) {
            String str = io.getWord();
            for (int x = 0; x < c; ++x) { map[y][x] = str.charAt(x); }
        }
    }

    //Dir: N=0, E=1, S=2
    private int dfs(int nodeR, int nodeC, int dir) {
        if (dir != 2 && nodeR > 0 && map[nodeR-1][nodeC] != '#' && visitedEdge[nodeR][nodeC][0] == 0) { //North
            visitedEdge[nodeR][nodeC][0] = 1;
            memory[nodeR][nodeC][0] = dfs(nodeR-1, nodeC, 0);
        }
        if (nodeC < c-1 && map[nodeR][nodeC+1] != '#' && visitedEdge[nodeR][nodeC][1] == 0) { //East
            visitedEdge[nodeR][nodeC][1] = 1;
            memory[nodeR][nodeC][1] = dfs(nodeR, nodeC+1, 1);
        }
        if (dir != 0 && nodeR < r-1 && map[nodeR+1][nodeC] != '#' && nodeC < c-1 && visitedEdge[nodeR][nodeC][2] == 0) { //South
            visitedEdge[nodeR][nodeC][2] = 1;
            memory[nodeR][nodeC][2] = dfs(nodeR+1, nodeC, 2);
        }

        if (dir == 0) { return memory[nodeR][nodeC][0] + memory[nodeR][nodeC][1]; }
        if (dir == 2) { return memory[nodeR][nodeC][2] + memory[nodeR][nodeC][1]; }
        else { return memory[nodeR][nodeC][0] + memory[nodeR][nodeC][1] + memory[nodeR][nodeC][2]; }
    }

    public static void main(String[] args) { new ConstrainedFreedomOfChoice(); }
}