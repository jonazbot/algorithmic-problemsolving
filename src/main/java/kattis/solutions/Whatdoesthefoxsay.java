// Whatdoesthefoxsay.java
// What Does the Fox Say? - open.kattis.com/problems/whatdoesthefoxsay
// By Jonas Valen

import java.util.*;

public class Whatdoesthefoxsay {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int cases = Integer.parseInt(sc.nextLine());
        String fox = "what does the fox say?";

        for (int testCase = 1; testCase <= cases; testCase++) {
            List<String> natureSounds = new ArrayList<>(Arrays.asList(sc.nextLine().split("\\s")));
            List<String> animalSounds = new ArrayList<>();
            while (true) {
                String temp = sc.nextLine();
                if (temp.equals(fox))
                        break;
                animalSounds.add(temp.split("\\s")[2]);
            }
            natureSounds.removeIf(animalSounds::contains);
            StringBuilder sb = new StringBuilder();
            for (String sounds:natureSounds)
                sb.append(sounds + " ");
            System.out.println(sb);
        }
    }
}


