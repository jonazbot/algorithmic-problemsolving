package kattis.library;

public class Edge<T> {

    private final T a, b;

    public Edge(T a, T b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) { return true; }
        if(obj==null) { return false; }
        if(obj instanceof Edge){
            Edge<T> edge = (Edge<T>) obj;
            return equals(edge.a, edge.b);
        }
        return false;
    }

    /**
     * Checks if this edge connects the two given nodes
     * @param a one node
     * @param b another node
     * @return true if the edge connects a and b
     */
    public boolean equals(T a, T b) {
        if(this.a.equals(a) && this.b.equals(b)) { return true; }
        if(this.a.equals(b) && this.b.equals(a)) { return true; }
        return false;
    }

    public T getA() { return this.a; }
    public T getB() { return this.b; }
    public String toString() { return String.format("%d, %d", a, b); }
}

