package kattis.library;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

/**
 * A data structure representing Disjoint-sets utilizing recursive path-compression and union-by-size, also called
 * Union-Find.
 *
 * @param <T> A generic type.
 */
public class DisjointSets<T> {
    private final HashMap<T,T> setRoots;
    private final HashMap<T,Integer> setSizes;

    /**
     * The constructor for DisjointSets.
     *
     * @param initialCapacity The initialCapacity for {@link HashMap}.
     */
    public DisjointSets(int initialCapacity) {
        this.setRoots = new HashMap<>(initialCapacity);
        this.setSizes = new HashMap<>(initialCapacity);
    }

    public void add(T elemA, T elemB) {
        if (!this.setRoots.containsKey(elemA)) {
            this.setRoots.put(elemA, elemA);
            this.setSizes.put(elemA, 1);
        }
        if (!this.setRoots.containsKey(elemB)) {
            this.setRoots.put(elemB, elemB);
            this.setSizes.put(elemB, 1);
        }
        union(elemA, elemB);
    }

    public void addOne(T elem) {
        if (!this.setRoots.containsKey(elem)) {
            this.setRoots.put(elem, elem);
            this.setSizes.put(elem, 1);
        }
    }

    /**
     * Adds all elements as root representatives of their respective disjoint-set with an initial size of 1.
     *
     * Time Complexity: O(N), where N is the number of elements.
     *
     * @param elements The {@link Iterable} elements to add as disjoint-sets.
     */
    public void makeSets(Collection<T> elements) {
        for(T elem : elements) {
            this.setRoots.put(elem, elem);
            this.setSizes.put(elem, 1);
        }
    }

    /**
     * Finds the root (representative) of the set the element belongs to and compresses the path by
     * recursively pointing each parent to the root element of the set.
     *
     * Time Complexity: O(log N), where N is the number of elements. Result of path compression and union-by-size.
     *
     * @param elem The element to find it's set representative.
     * @return The root element representing the set.
     */
    public T find(T elem) {
        if (!this.setRoots.get(elem).equals(elem)) { this.setRoots.put(elem, find(this.setRoots.get(elem))); }
        return this.setRoots.get(elem);
    }

    /**
     * Performs union of the sets of two elements, unless they are already in the same set, by finding
     * (and compressing paths to) each element's root and making the larger set's root node the parent
     * of the smaller set's root and adding the set size of the smaller set to the larger set.
     *
     * Time Complexity: O(log N), where N is the number of elements. Result of path compression and union-by-size.
     *
     * @param elemA The element who's set to union.
     * @param elemB The other element who's set to union.
     */
    public void union(T elemA, T elemB) {
        T rootA = find(elemA);
        T rootB = find(elemB);
        if (!rootA.equals(rootB)) {
            if (this.setSizes.get(find(rootA)) < this.setSizes.get(find(rootB))) {
                this.setRoots.put(rootA, rootB);
                this.setSizes.put(rootB, this.setSizes.get(rootA) + this.setSizes.get(rootB));
            }
            else {
                this.setRoots.put(rootB, rootA);
                this.setSizes.put(rootA, this.setSizes.get(rootA) + this.setSizes.get(rootB));
            }
        }
    }


    public HashMap<T, HashSet<T>> getSets() {
        HashMap<T, HashSet<T>> sets = new HashMap<>();
        for (T elem : setRoots.keySet()) {
            sets.computeIfAbsent(setRoots.get(elem), val -> new HashSet<>());
            sets.get(setRoots.get(elem)).add(elem); }
        return sets;
    }

    public HashMap<T, T> getRoots() { return this.setRoots; }

    public HashMap<T, Integer> getSizes() { return this.setSizes; }

}