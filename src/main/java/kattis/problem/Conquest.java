package kattis.problem;

import kattis.library.Kattio;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

public class Conquest {

    private final int[] armySize;
    private final PriorityQueue<Integer> pathOfLeastResistance;
    private final Map<Integer, Set<Integer>> bridges;
    private final Set<Integer> conquered;

    public Conquest() {
        Kattio io = new Kattio(System.in, System.out);
        int n = io.getInt();
        int m = io.getInt();
        armySize = new int[n+1];
        this.bridges = new HashMap<>(m);
        this.conquered = new HashSet<>(n);
        this.pathOfLeastResistance = new PriorityQueue<>(n, (a, b) -> {
            if (armySize[a] < armySize[b]) return -1;
            else if (armySize[a] > armySize[b]) return 1;
            return 0;
        });

        for (int i = 0; i < m; ++i) {
            int a = io.getInt();
            int b = io.getInt();
            bridges.computeIfAbsent(a, value -> new HashSet<>());
            bridges.get(a).add(b);
            bridges.computeIfAbsent(b, value -> new HashSet<>());
            bridges.get(b).add(a);
        }
        for (int i = 1; i <= n; ++i) { armySize[i] = io.getInt(); }

        io.print(conquer());
        io.close();
    }

    private int conquer() {
        armySize[0] += armySize[1];
        pathOfLeastResistance.addAll(bridges.get(1));
        conquered.add(1);
        while (!pathOfLeastResistance.isEmpty()) {
            int island = pathOfLeastResistance.poll();
            if (armySize[0] > armySize[island]) {
                armySize[0] += armySize[island];
                conquered.add(island);
                bridges.get(island).removeAll(conquered);
                pathOfLeastResistance.addAll(bridges.get(island));
            }
            else { break; }
        }
        return armySize[0];
    }

    public static void main(String[] args) { new Conquest(); }
}
