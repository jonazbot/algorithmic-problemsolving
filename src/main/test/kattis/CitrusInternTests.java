package main.test;

import DynamicProgramming2.CitrusIntern;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CitrusInternTests {

    @Test
    public void TestSample1() {
        int nodes = 8;
        List<List<Integer>> input = List.of(
                new ArrayList<>(List.of(2, 2, 1, 2)),
                new ArrayList<>(List.of(4, 0)),
                new ArrayList<>(List.of(3, 0)),
                new ArrayList<>(List.of(20, 1, 6)),
                new ArrayList<>(List.of(20, 1, 3)),
                new ArrayList<>(List.of(3, 2, 0, 4)),
                new ArrayList<>(List.of(5, 1, 7)),
                new ArrayList<>(List.of(1, 0)) );
        CitrusIntern intern = new CitrusIntern(nodes, input);
        assertEquals(15, intern.infiltrate(5));
    }

}
