package main.test;

import DynamicProgramming2.RebelPortals;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RebelPortalsTests {

    @Test
    public void TestSample1() {
        int nodes = 4;
        List<List<Integer>> input = List.of(
                new ArrayList<>(List.of(0, 0, 1)),
                new ArrayList<>(List.of(0, 1, 1)),
                new ArrayList<>(List.of(2, 0, 3)),
                new ArrayList<>(List.of(2, 1, 3)));
        RebelPortals portals = new RebelPortals(nodes, input);
        assertEquals(2.0, portals.getMinTravel());
    }

    @Test
    public void TestSample2() {
        int nodes = 6;
        List<List<Integer>> input = List.of(
                new ArrayList<>(List.of(0, 0, 0)),
                new ArrayList<>(List.of(50, 0, 0)),
                new ArrayList<>(List.of(0, 50, 0)),
                new ArrayList<>(List.of(50, 50, 0)),
                new ArrayList<>(List.of(24, 24, 0)),
                new ArrayList<>(List.of(24, 26, 0)));
        RebelPortals portals = new RebelPortals(nodes, input);
        assertEquals(102.0d, portals.getMinTravel());
    }

    @Test
    public void TestSample4() {
        int nodes = 18;
        List<List<Integer>> input = List.of(
                new ArrayList<>(List.of(314, 343, 237)),
                new ArrayList<>(List.of(394, 239, 183)),
                new ArrayList<>(List.of(49, 243, 164)),
                new ArrayList<>(List.of(300, 299, 89)),
                new ArrayList<>(List.of(304, 164, 207)),
                new ArrayList<>(List.of(366, 194, 368)),
                new ArrayList<>(List.of(88, 256, 55)),
                new ArrayList<>(List.of(446, 319, 338)),
                new ArrayList<>(List.of(471, 335, 249)),
                new ArrayList<>(List.of(471, 151, 5)),
                new ArrayList<>(List.of(476, 11, 334)),
                new ArrayList<>(List.of(443, 138, 301)),
                new ArrayList<>(List.of(491, 227, 406)),
                new ArrayList<>(List.of(69, 401, 217)),
                new ArrayList<>(List.of(115, 327, 79)),
                new ArrayList<>(List.of(345, 22, 484)),
                new ArrayList<>(List.of(98, 128, 35)),
                new ArrayList<>(List.of(334, 365, 242))
        );
        RebelPortals portals = new RebelPortals(nodes, input);
        assertEquals(1301.063446963372d, portals.getMinTravel());
    }

    @Test
    public void Test4Nodes() {
        int nodes = 4;
        List<List<Integer>> input = List.of(
                new ArrayList<>(List.of(0, 0, 0)),
                new ArrayList<>(List.of(1, 0, 0)),
                new ArrayList<>(List.of(2, 0, 0)),
                new ArrayList<>(List.of(3, 0, 0))
        );
        RebelPortals portals = new RebelPortals(nodes, input);
        assertEquals(2.0d, portals.getMinTravel());
    }

    @Test
    public void Test5Nodes() {
        int nodes = 5;
        List<List<Integer>> input = List.of(
                new ArrayList<>(List.of(0, 0, 0)),
                new ArrayList<>(List.of(1, 0, 0)),
                new ArrayList<>(List.of(2, 0, 0)),
                new ArrayList<>(List.of(3, 0, 0)),
                new ArrayList<>(List.of(4, 0, 0))
        );
        RebelPortals portals = new RebelPortals(nodes, input);
        assertEquals(2.0d, portals.getMinTravel());
    }

    @Test
    public void Test6Nodes() {
        int nodes = 6;
        List<List<Integer>> input = List.of(
                new ArrayList<>(List.of(0, 0, 0)),
                new ArrayList<>(List.of(1, 0, 0)),
                new ArrayList<>(List.of(2, 0, 0)),
                new ArrayList<>(List.of(3, 0, 0)),
                new ArrayList<>(List.of(4, 0, 0)),
                new ArrayList<>(List.of(5, 0, 0))
        );
        RebelPortals portals = new RebelPortals(nodes, input);
        assertEquals(3.0d, portals.getMinTravel());
    }

    @Test
    public void Test7Nodes() {
        int nodes = 7;
        List<List<Integer>> input = List.of(
                new ArrayList<>(List.of(0, 0, 0)),
                new ArrayList<>(List.of(1, 0, 0)),
                new ArrayList<>(List.of(2, 0, 0)),
                new ArrayList<>(List.of(3, 0, 0)),
                new ArrayList<>(List.of(4, 0, 0)),
                new ArrayList<>(List.of(5, 0, 0)),
                new ArrayList<>(List.of(6, 0, 0))
        );
        RebelPortals portals = new RebelPortals(nodes, input);
        assertEquals(3.0d, portals.getMinTravel());
    }

    @Test
    public void Test8Nodes() {
        int nodes = 8;
        List<List<Integer>> input = List.of(
                new ArrayList<>(List.of(0, 0, 0)),
                new ArrayList<>(List.of(1, 0, 0)),
                new ArrayList<>(List.of(2, 0, 0)),
                new ArrayList<>(List.of(3, 0, 0)),
                new ArrayList<>(List.of(4, 0, 0)),
                new ArrayList<>(List.of(5, 0, 0)),
                new ArrayList<>(List.of(6, 0, 0)),
                new ArrayList<>(List.of(7, 0, 0))
        );
        RebelPortals portals = new RebelPortals(nodes, input);
        assertEquals(4.0d, portals.getMinTravel());
    }

    @Test
    public void Test9Nodes() {
        int nodes = 9;
        List<List<Integer>> input = List.of(
                new ArrayList<>(List.of(0, 0, 0)),
                new ArrayList<>(List.of(1, 0, 0)),
                new ArrayList<>(List.of(2, 0, 0)),
                new ArrayList<>(List.of(3, 0, 0)),
                new ArrayList<>(List.of(4, 0, 0)),
                new ArrayList<>(List.of(5, 0, 0)),
                new ArrayList<>(List.of(6, 0, 0)),
                new ArrayList<>(List.of(7, 0, 0)),
                new ArrayList<>(List.of(8, 0, 0))
        );
        RebelPortals portals = new RebelPortals(nodes, input);
        assertEquals(4.0d, portals.getMinTravel());
    }

    @Test
    public void Test10Nodes() {
        int nodes = 10;
        List<List<Integer>> input = List.of(
                new ArrayList<>(List.of(0, 0, 0)),
                new ArrayList<>(List.of(1, 0, 0)),
                new ArrayList<>(List.of(2, 0, 0)),
                new ArrayList<>(List.of(3, 0, 0)),
                new ArrayList<>(List.of(4, 0, 0)),
                new ArrayList<>(List.of(5, 0, 0)),
                new ArrayList<>(List.of(6, 0, 0)),
                new ArrayList<>(List.of(7, 0, 0)),
                new ArrayList<>(List.of(8, 0, 0)),
                new ArrayList<>(List.of(9, 0, 0))
        );
        RebelPortals portals = new RebelPortals(nodes, input);
        assertEquals(5.0d, portals.getMinTravel());
    }

    @Test
    public void Test12Nodes() {
        int nodes = 12;
        List<List<Integer>> input = List.of(
                new ArrayList<>(List.of(0, 0, 0)),
                new ArrayList<>(List.of(1, 0, 0)),
                new ArrayList<>(List.of(2, 0, 0)),
                new ArrayList<>(List.of(3, 0, 0)),
                new ArrayList<>(List.of(4, 0, 0)),
                new ArrayList<>(List.of(5, 0, 0)),
                new ArrayList<>(List.of(6, 0, 0)),
                new ArrayList<>(List.of(7, 0, 0)),
                new ArrayList<>(List.of(8, 0, 0)),
                new ArrayList<>(List.of(9, 0, 0)),
                new ArrayList<>(List.of(10, 0, 0)),
                new ArrayList<>(List.of(11, 0, 0)),
                new ArrayList<>(List.of(12, 0, 0))
        );
        RebelPortals portals = new RebelPortals(nodes, input);
        assertEquals(6.0d, portals.getMinTravel());
    }

    @Test
    public void Test14Nodes() {
        int nodes = 14;
        List<List<Integer>> input = List.of(
                new ArrayList<>(List.of(0, 0, 0)),
                new ArrayList<>(List.of(1, 0, 0)),
                new ArrayList<>(List.of(2, 0, 0)),
                new ArrayList<>(List.of(3, 0, 0)),
                new ArrayList<>(List.of(4, 0, 0)),
                new ArrayList<>(List.of(5, 0, 0)),
                new ArrayList<>(List.of(6, 0, 0)),
                new ArrayList<>(List.of(7, 0, 0)),
                new ArrayList<>(List.of(8, 0, 0)),
                new ArrayList<>(List.of(9, 0, 0)),
                new ArrayList<>(List.of(10, 0, 0)),
                new ArrayList<>(List.of(11, 0, 0)),
                new ArrayList<>(List.of(12, 0, 0)),
                new ArrayList<>(List.of(13, 0, 0))
        );
        RebelPortals portals = new RebelPortals(nodes, input);
        assertEquals(7.0d, portals.getMinTravel());
    }

    @Test
    public void Test15Nodes() {
        int nodes = 15;
        List<List<Integer>> input = List.of(
                new ArrayList<>(List.of(0, 0, 0)),
                new ArrayList<>(List.of(1, 0, 0)),
                new ArrayList<>(List.of(2, 0, 0)),
                new ArrayList<>(List.of(3, 0, 0)),
                new ArrayList<>(List.of(4, 0, 0)),
                new ArrayList<>(List.of(5, 0, 0)),
                new ArrayList<>(List.of(6, 0, 0)),
                new ArrayList<>(List.of(7, 0, 0)),
                new ArrayList<>(List.of(8, 0, 0)),
                new ArrayList<>(List.of(9, 0, 0)),
                new ArrayList<>(List.of(10, 0, 0)),
                new ArrayList<>(List.of(11, 0, 0)),
                new ArrayList<>(List.of(12, 0, 0)),
                new ArrayList<>(List.of(13, 0, 0)),
                new ArrayList<>(List.of(14, 0, 0))
        );
        RebelPortals portals = new RebelPortals(nodes, input);
        assertEquals(7.0d, portals.getMinTravel());
    }

    @Test
    public void Test16Nodes() {
        int nodes = 16;
        List<List<Integer>> input = List.of(
                new ArrayList<>(List.of(0, 0, 0)),
                new ArrayList<>(List.of(1, 0, 0)),
                new ArrayList<>(List.of(2, 0, 0)),
                new ArrayList<>(List.of(3, 0, 0)),
                new ArrayList<>(List.of(4, 0, 0)),
                new ArrayList<>(List.of(5, 0, 0)),
                new ArrayList<>(List.of(6, 0, 0)),
                new ArrayList<>(List.of(7, 0, 0)),
                new ArrayList<>(List.of(8, 0, 0)),
                new ArrayList<>(List.of(9, 0, 0)),
                new ArrayList<>(List.of(10, 0, 0)),
                new ArrayList<>(List.of(11, 0, 0)),
                new ArrayList<>(List.of(12, 0, 0)),
                new ArrayList<>(List.of(13, 0, 0)),
                new ArrayList<>(List.of(14, 0, 0)),
                new ArrayList<>(List.of(15, 0, 0))
        );
        RebelPortals portals = new RebelPortals(nodes, input);
        assertEquals(8.0d, portals.getMinTravel());
    }

    @Test
    public void Test17Nodes() {
        int nodes = 17;
        List<List<Integer>> input = List.of(
                new ArrayList<>(List.of(0, 0, 0)),
                new ArrayList<>(List.of(1, 0, 0)),
                new ArrayList<>(List.of(2, 0, 0)),
                new ArrayList<>(List.of(3, 0, 0)),
                new ArrayList<>(List.of(4, 0, 0)),
                new ArrayList<>(List.of(5, 0, 0)),
                new ArrayList<>(List.of(6, 0, 0)),
                new ArrayList<>(List.of(7, 0, 0)),
                new ArrayList<>(List.of(8, 0, 0)),
                new ArrayList<>(List.of(9, 0, 0)),
                new ArrayList<>(List.of(10, 0, 0)),
                new ArrayList<>(List.of(11, 0, 0)),
                new ArrayList<>(List.of(12, 0, 0)),
                new ArrayList<>(List.of(13, 0, 0)),
                new ArrayList<>(List.of(14, 0, 0)),
                new ArrayList<>(List.of(15, 0, 0)),
                new ArrayList<>(List.of(16, 0, 0))
        );
        RebelPortals portals = new RebelPortals(nodes, input);
        assertEquals(8.0d, portals.getMinTravel());
    }

    @Test
    public void Test18Nodes() {
        int nodes = 18;
        List<List<Integer>> input = List.of(
                new ArrayList<>(List.of(0, 0, 0)),
                new ArrayList<>(List.of(1, 0, 0)),
                new ArrayList<>(List.of(2, 0, 0)),
                new ArrayList<>(List.of(3, 0, 0)),
                new ArrayList<>(List.of(4, 0, 0)),
                new ArrayList<>(List.of(5, 0, 0)),
                new ArrayList<>(List.of(6, 0, 0)),
                new ArrayList<>(List.of(7, 0, 0)),
                new ArrayList<>(List.of(8, 0, 0)),
                new ArrayList<>(List.of(9, 0, 0)),
                new ArrayList<>(List.of(10, 0, 0)),
                new ArrayList<>(List.of(11, 0, 0)),
                new ArrayList<>(List.of(12, 0, 0)),
                new ArrayList<>(List.of(13, 0, 0)),
                new ArrayList<>(List.of(14, 0, 0)),
                new ArrayList<>(List.of(15, 0, 0)),
                new ArrayList<>(List.of(16, 0, 0)),
                new ArrayList<>(List.of(17, 0, 0))
        );
        RebelPortals portals = new RebelPortals(nodes, input);
        assertEquals(9.0d, portals.getMinTravel());
    }

}
