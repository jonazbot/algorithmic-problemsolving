package main.test;

import org.junit.jupiter.api.Test;

import java.solutions.PegGameForTwo;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PegGameForTwoTests {

    @Test
    public void test1() {
        int[][] board = {
                new int[] {3},
                new int[] {1, 6},
                new int[] {1 ,7, 8},
                new int[] {5, 0, 3, 4},
                new int[] {9, 3, 2, 1, 9} };

        PegGameForTwo game = new PegGameForTwo();
        assertEquals(21, game.play(board, true));
    }

    @Test
    public void test2() {
        int[][] board = {
                new int[] {1},
                new int[] {2, 3},
                new int[] {4 ,5, 6},
                new int[] {7, 8, 9, 10},
                new int[] {11, 12, 0, 13, 14} };

        PegGameForTwo game = new PegGameForTwo();
        assertEquals(19, game.play(board, true));
    }

    @Test
    public void test3() {
        int[][] board = {
                new int[] {100},
                new int[] {1, 17},
                new int[] {99 ,3, 4},
                new int[] {0, 76, 33, 42},
                new int[] {12, 13, 14, 15, 16} };

        PegGameForTwo game = new PegGameForTwo();
        assertEquals(2148, game.play(board, true));
    }
}
