package main.test;

import ExponentialTime.Coloring;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ColoringGraphsTests {

    @Test
    public void TestSample1() {
        int nodes = 4;
        Map<Integer, List<Integer>> graph = Map.ofEntries(
                new HashMap.SimpleEntry<Integer, List<Integer>>(0, new ArrayList<>(List.of(1, 2))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(1, new ArrayList<>(List.of(0, 2, 3))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(2, new ArrayList<>(List.of(0, 1))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(3, new ArrayList<>(List.of(1)))
        );
        HashMap<Integer, Integer> colors = new HashMap<>(nodes);
        Coloring coloringTest = new Coloring(nodes, graph, colors);

        assertEquals(3, coloringTest.findK());
    }

    @Test
    public void TestSample2() {
        int nodes = 5;
        Map<Integer, List<Integer>> graph = Map.ofEntries(
                new HashMap.SimpleEntry<Integer, List<Integer>>(0, new ArrayList<>(List.of(2, 3, 4))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(1, new ArrayList<>(List.of(2, 3, 4))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(2, new ArrayList<>(List.of(0, 1))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(3, new ArrayList<>(List.of(0, 1))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(4, new ArrayList<>(List.of(0, 1)))
        );
        HashMap<Integer, Integer> colors = new HashMap<>(nodes);
        Coloring coloringTest = new Coloring(nodes, graph, colors);

        assertEquals(2, coloringTest.findK());
    }

    @Test
    public void TestSample3() {
        int nodes = 6;
        Map<Integer, List<Integer>> graph = Map.ofEntries(
                new HashMap.SimpleEntry<Integer, List<Integer>>(0, new ArrayList<>(List.of(1, 3))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(1, new ArrayList<>(List.of(0, 2, 4))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(2, new ArrayList<>(List.of(1, 5))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(3, new ArrayList<>(List.of(0, 4))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(4, new ArrayList<>(List.of(1, 3, 5))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(5, new ArrayList<>(List.of(2, 4)))
        );
        HashMap<Integer, Integer> colors = new HashMap<>(nodes);
        Coloring coloringTest = new Coloring(nodes, graph, colors);

        assertEquals(2, coloringTest.findK());
    }

    @Test
    public void TestSample4() {
        int nodes = 4;
        Map<Integer, List<Integer>> graph = Map.ofEntries(
                new HashMap.SimpleEntry<Integer, List<Integer>>(0, new ArrayList<>(List.of(1, 2, 3))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(1, new ArrayList<>(List.of(0, 2, 3))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(2, new ArrayList<>(List.of(0, 1, 3))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(3, new ArrayList<>(List.of(0, 1, 2)))
        );
        HashMap<Integer, Integer> colors = new HashMap<>(nodes);
        Coloring coloringTest = new Coloring(nodes, graph, colors);

        assertEquals(4, coloringTest.findK());
    }

    @Test
    public void TestSample5() {
        int nodes = 5;
        Map<Integer, List<Integer>> graph = Map.ofEntries(
                new HashMap.SimpleEntry<Integer, List<Integer>>(0, new ArrayList<>(List.of(1, 2))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(1, new ArrayList<>(List.of(0, 2, 3))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(2, new ArrayList<>(List.of(0, 1, 4))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(3, new ArrayList<>(List.of(1, 4))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(4, new ArrayList<>(List.of(2, 3)))
        );
        HashMap<Integer, Integer> colors = new HashMap<>(nodes);
        Coloring coloringTest = new Coloring(nodes, graph, colors);

        assertEquals(3, coloringTest.findK());
    }

    @Test
    public void TestMin() {
        int nodes = 2;
        Map<Integer, List<Integer>> graph = Map.ofEntries(
                new HashMap.SimpleEntry<Integer, List<Integer>>(0, new ArrayList<>(List.of(1))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(1, new ArrayList<>(List.of(0)))
        );
        HashMap<Integer, Integer> colors = new HashMap<>(nodes);
        Coloring coloringTest = new Coloring(nodes, graph, colors);

        assertEquals(2, coloringTest.findK());
    }

    @Test
    public void TestTree3() {
        int nodes = 3;
        Map<Integer, List<Integer>> graph = Map.ofEntries(
                new HashMap.SimpleEntry<Integer, List<Integer>>(0, new ArrayList<>(List.of(1, 2))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(1, new ArrayList<>(List.of(0, 2))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(2, new ArrayList<>(List.of(0, 1)))
        );
        HashMap<Integer, Integer> colors = new HashMap<>(nodes);
        Coloring coloringTest = new Coloring(nodes, graph, colors);

        assertEquals(3, coloringTest.findK());
    }

    @Test
    public void TestMinCycle() {
        int nodes = 3;
        Map<Integer, List<Integer>> graph = Map.ofEntries(
                new HashMap.SimpleEntry<Integer, List<Integer>>(0, new ArrayList<>(List.of(1))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(1, new ArrayList<>(List.of(0,2))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(2, new ArrayList<>(List.of(1)))
        );
        HashMap<Integer, Integer> colors = new HashMap<>(nodes);
        Coloring coloringTest = new Coloring(nodes, graph, colors);

        assertEquals(2, coloringTest.findK());
    }

    @Test
    public void TestBipartiteOrder1() {
        int nodes = 8;
        Map<Integer, List<Integer>> graph = Map.ofEntries(
                new HashMap.SimpleEntry<Integer, List<Integer>>(0, new ArrayList<>(List.of(5, 6, 7))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(1, new ArrayList<>(List.of(4, 6, 7))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(2, new ArrayList<>(List.of(4, 5, 7))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(3, new ArrayList<>(List.of(4, 5, 6))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(4, new ArrayList<>(List.of(1, 2, 3))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(5, new ArrayList<>(List.of(0, 2, 3))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(6, new ArrayList<>(List.of(0, 1, 3))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(7, new ArrayList<>(List.of(0, 1, 2)))
        );
        HashMap<Integer, Integer> colors = new HashMap<>(nodes);
        Coloring coloringTest = new Coloring(nodes, graph, colors);

        assertEquals(2, coloringTest.findK());
    }

    @Test
    public void TestBipartiteOrder2() {
        int nodes = 8;
        Map<Integer, List<Integer>> graph = Map.ofEntries(
                new HashMap.SimpleEntry<Integer, List<Integer>>(0, new ArrayList<>(List.of(3, 5, 7))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(1, new ArrayList<>(List.of(2, 4, 6))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(2, new ArrayList<>(List.of(1, 5, 7))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(3, new ArrayList<>(List.of(0, 4, 6))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(4, new ArrayList<>(List.of(1, 3, 7))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(5, new ArrayList<>(List.of(0, 2, 6))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(6, new ArrayList<>(List.of(1, 3, 5))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(7, new ArrayList<>(List.of(0, 2, 4)))
        );
        HashMap<Integer, Integer> colors = new HashMap<>(nodes);
        Coloring coloringTest = new Coloring(nodes, graph, colors);

        assertEquals(2, coloringTest.findK());
    }

    @Test
    public void TestCase6() {
        int nodes = 11;
        Map<Integer, List<Integer>> graph = Map.ofEntries(
                new HashMap.SimpleEntry<Integer, List<Integer>>(0, new ArrayList<>(List.of(1, 2, 3, 4, 7, 9, 10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(1, new ArrayList<>(List.of(0, 2, 4, 5, 7, 8, 9, 10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(2, new ArrayList<>(List.of(0, 1, 3, 5, 6, 7, 8, 9, 10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(3, new ArrayList<>(List.of(0, 2, 4, 6, 9, 10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(4, new ArrayList<>(List.of(0, 1, 3, 7, 8, 10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(5, new ArrayList<>(List.of(1, 2, 7, 9, 10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(6, new ArrayList<>(List.of(2, 3, 7, 8, 9, 10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(7, new ArrayList<>(List.of(0, 1, 2, 4, 5, 6, 8, 10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(8, new ArrayList<>(List.of(1, 2, 4, 6, 7, 9, 10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(9, new ArrayList<>(List.of(0, 1, 2, 3, 5, 6, 8))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(10, new ArrayList<>(List.of(0, 1, 2, 3, 4, 5, 6, 7, 8)))
        );
        HashMap<Integer, Integer> colors = new HashMap<>(nodes);
        Coloring coloringTest = new Coloring(nodes, graph, colors);

        assertEquals(5, coloringTest.findK());
    }
    @Test
    public void TestCaseX() {
        int nodes = 11;
        Map<Integer, List<Integer>> graph = Map.ofEntries(
                new HashMap.SimpleEntry<Integer, List<Integer>>(0, new ArrayList<>(List.of(1, 3))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(1, new ArrayList<>(List.of(0, 2, 3, 5, 7, 9))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(2, new ArrayList<>(List.of(1, 3))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(3, new ArrayList<>(List.of(0, 1, 2, 4, 5, 6))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(4, new ArrayList<>(List.of(3, 8, 9, 10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(5, new ArrayList<>(List.of(1, 3, 8))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(6, new ArrayList<>(List.of(3))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(7, new ArrayList<>(List.of(1, 10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(8, new ArrayList<>(List.of(4, 5))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(9, new ArrayList<>(List.of(1, 4))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(10, new ArrayList<>(List.of(4, 7)))
        );
        HashMap<Integer, Integer> colors = new HashMap<>(nodes);
        Coloring coloringTest = new Coloring(nodes, graph, colors);

        assertEquals(3, coloringTest.findK());
    }

    @Test
    public void TestPetersen() {
        int nodes = 10;
        Map<Integer, List<Integer>> graph = Map.ofEntries(
                new HashMap.SimpleEntry<Integer, List<Integer>>(0, new ArrayList<>(List.of(1, 3, 8))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(1, new ArrayList<>(List.of(0, 4, 2))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(2, new ArrayList<>(List.of(1, 5, 9))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(3, new ArrayList<>(List.of(0, 5, 7))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(4, new ArrayList<>(List.of(1, 6, 7))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(5, new ArrayList<>(List.of(2, 3, 6))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(6, new ArrayList<>(List.of(8, 4, 5))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(7, new ArrayList<>(List.of(9, 3, 4))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(8, new ArrayList<>(List.of(0, 6, 9))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(9, new ArrayList<>(List.of(8, 7, 2)))
        );
        HashMap<Integer, Integer> colors = new HashMap<>(nodes);
        Coloring coloringTest = new Coloring(nodes, graph, colors);

        assertEquals(3, coloringTest.findK());
    }

    @Test
    public void TestCase14() {
        int nodes = 11;
        Map<Integer, List<Integer>> graph = Map.ofEntries(
                new HashMap.SimpleEntry<Integer, List<Integer>>(0, new ArrayList<>(List.of(4, 6))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(1, new ArrayList<>(List.of(2, 3, 10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(2, new ArrayList<>(List.of(1, 9, 10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(3, new ArrayList<>(List.of(1, 6))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(4, new ArrayList<>(List.of(0, 6, 7, 10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(5, new ArrayList<>(List.of(8, 10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(6, new ArrayList<>(List.of(0, 3, 4, 9, 10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(7, new ArrayList<>(List.of(4))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(8, new ArrayList<>(List.of(5))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(9, new ArrayList<>(List.of(2, 6))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(10, new ArrayList<>(List.of(1, 2, 4, 5, 6)))
        );
        HashMap<Integer, Integer> colors = new HashMap<>(nodes);
        Coloring coloringTest = new Coloring(nodes, graph, colors);

        assertEquals(3, coloringTest.findK());
    }

    @Test
    public void TestK11() {
        int nodes = 11;
        Map<Integer, List<Integer>> graph = Map.ofEntries(
                new HashMap.SimpleEntry<Integer, List<Integer>>(0, new ArrayList<>(List.of(1,2,3,4,5,6,7,8,9,10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(1, new ArrayList<>(List.of(0,2,3,4,5,6,7,8,9,10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(2, new ArrayList<>(List.of(0,1,3,4,5,6,7,8,9,10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(3, new ArrayList<>(List.of(0,1,2,4,5,6,7,8,9,10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(4, new ArrayList<>(List.of(0,1,2,3,5,6,7,8,9,10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(5, new ArrayList<>(List.of(0,1,2,3,4,6,7,8,9,10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(6, new ArrayList<>(List.of(0,1,2,3,4,5,7,8,9,10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(7, new ArrayList<>(List.of(0,1,2,3,4,5,6,8,9,10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(8, new ArrayList<>(List.of(0,1,2,3,4,5,6,7,9,10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(9, new ArrayList<>(List.of(0,1,2,3,4,5,6,7,8,10))),
                new HashMap.SimpleEntry<Integer, List<Integer>>(10, new ArrayList<>(List.of(0,1,2,3,4,5,6,7,8,9)))
        );
        HashMap<Integer, Integer> colors = new HashMap<>(nodes);
        Coloring coloringTest = new Coloring(nodes, graph, colors);

        assertEquals(11, coloringTest.findK());
    }
}
